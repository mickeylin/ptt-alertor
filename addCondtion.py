import json
import sys, argparse
from model import PttAlertor
from model import session as db

add_list = [
        (1, "Lifeismoney", "[情報]", 43524),
        (2, "Steam", "[限免]", 895)
]

def main(parser):
    args = parser.parse_args()
    if args.add and not args.id and not args.num:
        addCondition()
    elif args.modify and args.id and args.num:
        modifyCondition(args.id, args.num)
    else:
        parser.print_help()
    printAll()


def printAll():
    qs = db.query(PttAlertor).all()
    print(json.dumps(json.loads(str(qs)), indent=2, ensure_ascii=False))


def addCondition():
    new_alert_datas = []
    for id, board_name, condition, newest_index in add_list:
        if db.query(PttAlertor).filter_by(id = id).first():
            print(f"{id} is exist.")
        else:
            new_alert_datas.append(PttAlertor(id = id,
                                             board_name = board_name,
                                             condition = condition,
                                             newest_index = newest_index))
    if new_alert_datas:
        db.add_all(new_alert_datas)
        db.commit()
        for data in new_alert_datas:
            print(f"{data.board_name} {data.condition} added.")


def modifyCondition(id, post_index):
    qs = db.query(PttAlertor).filter_by(id = id).first()
    if qs:
        qs.newest_index = post_index
        db.commit()
    else:
        print(f"{id} is not exist")


if __name__ == "__main__":
    parser = argparse.ArgumentParser("dbwrapper.py")
    group = parser.add_mutually_exclusive_group()
    group.add_argument("-a", "--add", help="Add new condition", action="store_true")
    group.add_argument("-m", "--modify", help="Modify specified entry", action="store_true")
    parser.add_argument("--id", help="post id in database", type=int)
    parser.add_argument("--num", help="post number", type=int)
    main(parser)
