import os, sys, json, requests
import config as cfg
from PyPtt import PTT
from model import PttAlertor
from model import session as db
from time import sleep


ptt_bot = PTT.API()

def main():
    login()
    crawl_newest_post()
    #while 1:
    #    try:
    #        crawl_newest_post()
    #    except Exception as e:
    #        print(e)
    #    sleep(30)


def crawl_newest_post():
    # Get each entry from db
    for entry in db.query(PttAlertor).all():
      # Get newest index from ptt for each entry
        cur_newest_index = ptt_bot.get_newest_index(
            PTT.data_type.index_type.BBS,
            entry.board_name,
            search_type = PTT.data_type.post_search_type.KEYWORD,
            search_condition = entry.condition
        )
      # Get all post index range from current in db to newest in ptt
        for idx in range(entry.newest_index+1, cur_newest_index+1):
            post = ptt_bot.get_post(
                entry.board_name,
                post_index = idx,
                search_type = PTT.data_type.post_search_type.KEYWORD,
                search_condition = entry.condition,
                query = True
            )
          # Post is deleted during this searching 
            #if post.title is None:
            #    cur_newest_index = entry.newest_index
            #    break
            send_to_tg(f"{post.title}\n{post.web_url}")
            ptt_bot.log(f"{entry.board_name} {idx} {post.title}")
      # commit to db
        entry.newest_index = cur_newest_index
        db.commit()


def send_to_tg(text):
    messege = {
            "chat_id": cfg.chat_id,
            "text": text
           }
    api_res = requests.get(cfg.tg_url, params=messege)


def login():
    try:
        ptt_bot.login(cfg.ptt_id, cfg.ptt_password)
    except PTT.exceptions.LoginError:
        ptt_bot.log('登入失敗')
        sys.exit()
    except PTT.exceptions.WrongIDorPassword:
        ptt_bot.log('帳號密碼錯誤')
        sys.exit()
    except PTT.exceptions.LoginTooOften:
        ptt_bot.log('請稍等一下再登入')
        sys.exit()
    #ptt_bot.log('登入成功')
    #send_to_tg('登入成功')


if __name__ == "__main__":
    main()
