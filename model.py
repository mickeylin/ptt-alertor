import sqlalchemy
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import sessionmaker
from config import db_file

engine = sqlalchemy.create_engine(f"sqlite:///{db_file}", echo=False)
Session = sessionmaker(bind=engine)
session = Session()
Base = declarative_base()

class PttAlertor(Base):
    __tablename__ = 'crawl_data'

    id = Column(Integer,
                nullable=False,
                primary_key=True)
    board_name = Column(String(50),
                nullable=False)
    condition = Column(String(50),
                nullable=False)
    newest_index = Column(Integer,
                nullable=False)

    def __repr__(self):
        return f'{{ "id": {self.id}, "baord_name": "{self.board_name}", "condition": "{self.condition}", "newest_index": "{self.newest_index}"}}'
